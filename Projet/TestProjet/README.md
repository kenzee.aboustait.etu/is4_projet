>> Aboustait Kenzée
>> Maillé Augustin

# Mini-Projet Test & Maintenance

> Ce document est un compte rendu de ce qui a été réalisé pour le projet de test et maintenance.

## Rappel Contexte

L’objectif est de faire un petit programme simple de traitement d’un fichier `CSV` qui contient une liste de chercheurs et d’en fournir un sous-ensemble au format `YAML` ou `JSON`. Ce mini-projet visera à utiliser toutes les techniques de tests, maintenance et _clean code_ vues en cours.

## Données sources

Le fichier de données `csrankings.csv` vient du site [CSRankings: Computer Science Rankings](https://csrankings.org)] qui cherche à classer les chercheurs et institutions du monde en fonction de leur thématiques de recherche. Le site propose un moteur de recherche mais fournit également les données brutes sous la forme d’un tableau contenant les colonnes suivantes:

```csv
name,affiliation,homepage,scholarid
```

où:
-  `name` représente le chercheur sous la forme _Prénom Nom_
-  `affiliation` représente  l’institut qui héberge le chercheur
-  `homepage` la page par défaut du chercheur
-  `scholarid` son identifiant sur le site [Google Scholar](https://scholar.google.fr/).  Par exemple un chercheur ayant l’identifiant `pzwDiKgAAAAJ` aura comme page sur google scholar la page suivante [François Boulier - Google Scholar](https://scholar.google.fr/citations?user=pzwDiKgAAAAJ). Au maximum l’identifiant fait 13 caractères (12 caractères dans le cas classique et 13 caractères lorsqu’il n’y pas de profil google scholar dans ce cas l’identifiant est `NOSCHOLARPAGE`).

## Arborescence


Notre rendu est organisé de la façon suivante :
```
    - TestProjet/
        -  build/
            - correct.yaml
            - fichiers output
        - includes/
            - fonctionsBases.h
            - json.h
            - yaml.h
        - scr/
            - fonctionsBases.c
            - json.c
            - yaml.c
            - main.c
        - tests/
            - correct.yaml
            - test_units.c
        - csrankings.csv
        - makefile
        - .pre-commit-config.yaml
        -README.md

```

## Commandes d'execution


Ouvrir un terminal dans le dossier TestProjet (à la racine) et exectuter les commandes suivantes.

### Programme

```csv
$ make all
$ ./build/programme -t yaml -o nomFichier.yaml -f nahs -s "University of Hildesheim"
```
La liste des options possibles est la suivante:

- `-t [yaml, json]`: spécifie le format de sortie. Si cette option n'est pas utilisée, la valeur par défaut est `yaml`.
- `-o [nom_fichier]`: permet d'enregistrer le résultat dans le fichier spécifié. Si cette option n'est pas utilisée, le programme affiche le résultat sur la sortie standard.
- `-f [nahs]`: permet de sélectionner les informations à afficher par chercheur (`n` pour le nom, `a` pour l'affiliation, `h` pour la homepage, `s` pour le profil google scholar). Plusieurs filtres peuvent être utilisés par exemple `-f n -f a` ou `-f na` doivent permettre d'afficher les noms et les affiliations des chercheurs.
- `-s "Lieu Affiliation`: permet de rechercher uniquement les chercheurs affiliés à une institution. Les guillements sont à utiliser uniquement quand l'affiliation contient des espaces comme par exemple `-s "University of Hildesheim"`.

## Tests unitaires et d'intégration

Toujours dans un terminal à la racine de l'arborescence :

```csv
$ make all
$ ./build/test
```

## Pre- commit

Dans le même terminal, exectuer les lignes suivantes :

```csv
$ pip install pre-commit
$ pre-commit --version
$ pre commit install
$ pre-commit run --all-files
```




## pipeline

A executer directement sur gitlab. 


# Travail effectué

Pour le rendu de ce projet, nous avons commencé à rédiger un programme prenant en entrée différentes commandes afin qu'il produise les rendus attendu. Le programme permet d'ecrire soit dans le terminal, soit dans un fichier, les individus et leur caractèristiques selon une base de données. Les rendus peuvent être au format yaml ou json. Les données en sortie peuvent également être filtrées en fonction de la valeur **affiliation** donnée à l'éxecution. C'est l'utilisateur qui choisit en fonction de la commande d'execution les paramètres souhaités.
Nous pouvons obtenir les resultats de ce programme via les commandes expliquées plus tôt.

Une fois ce fichier fonctionnel, nous avont décidé de le séparer en plusieurs fihciers afin que cela soit plus compréhensible. A l'aide du fichier de base main.c, nous avons alors séparé notre programme en trois nouveaux fichier : un fichier contenant les fonction de bases, un fichier permettant l'écriture (terminal et fichier) en format json, le dernier l'écriture (terminal et fichier) en format yaml. Pour chacun de ces fichiers c, nous avons créé le fichier .h correspondant. Les fichiers c se trouvent dans l'Arborescence *TestProjet/scr/* et les .h dans *TestProjet/incldudes*.

Nous avons ensuite verifié que les sorties respectaient bien les formats json et yaml en utilisant `yamllint` et `jsonlint`.

Pour les tests unitaires, nous avons repris chacune des fonctions réalisées dans l'ensemble de nos programmes et avons réalisé des tests. Ces fonctions de test se trouvent dans */TestProjet/tests/test_units.c*. Ainsi, chacune de nos fonctions est testée.
Dans le même fichier, on trouve également un test d'intégration. Celui ci permet de comparer un fichier yaml créé par nos fonctions avec un fichier correct. Pour cela, un fichier *correct.yaml* est placé dans le dossier /tests.

Nous avons ensuite créé un ficher *makefile* permettant la compilation automatique `cf $ make all`. Les fichiers output seront stockés dans le dossier /build/ et permettront l'execution de nos programmes : `$ ./build/programme` ou `$ ./build/tests`

Le fichier `.pre-commit-config.yaml` rédigé permet la validation des commits.

Enfin nous avons mis en place une pipeline d'intégration.
Il est composé de 5 stage : 
- build : permet de tout compiler
- run : permet de run le programme
- test : effectue les tests
- coverage : pour obtenir la couverture du code 
- clean







