#ifndef JSON_H
#define JSON_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <fonctionsBases.h>


void print_par_affiliation_json(int *, char*, int );
void print_elem_json(int * ,char* , int );
void write_elem_json(int * ,char* , int , FILE *);
void write_par_affiliation_json(int *, char*, int , FILE * );

#endif /* JSON_H*/
