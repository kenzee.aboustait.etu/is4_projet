#ifndef YAML_H
#define YAML_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <fonctionsBases.h>

void print_par_affiliation_yaml(int *, char*);
void print_elem_yaml(int * ,char* );
void write_elem_yaml(int * ,char* , FILE *);
void write_par_affiliation_yaml(int *, char*, FILE * );

#endif /* YAML_H*/
