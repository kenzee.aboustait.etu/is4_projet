#ifndef FONCTIONSBASES_H
#define FONCTIONSBASES_H

#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>


void strip(char *);
void removeChar(char *, char);
char* getfield(char* , int );
char * get_elem(char *, int );
int* which_column(char* );

#endif /* FONCTIONSBASES_H */
