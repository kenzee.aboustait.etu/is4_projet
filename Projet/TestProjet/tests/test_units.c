#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <assert.h>
//#include <test_units.h>
#include <fonctionsBases.h>
#include <json.h>
#include <yaml.h>

#define TEST_YAML "test.yaml"
#define CORRECT_YAML "correct.yaml"
// Fonction permettant de tester la fonction strip
void test_strip() {
    char s[20] = "test\ting";
    strip(s);
    assert(strcmp(s, "testing") == 0);

    char s2[20] = "\tstrip\ttest";
    strip(s2);
    assert(strcmp(s2, "striptest") == 0);

    char s3[20] = "";
    strip(s3);
    assert(strcmp(s3, "") == 0);
}

// Fonction permettant de tester la fonction removeChar
void test_removeChar() {
    char str[20] = "removeChar";
    removeChar(str, 'e');
    assert(strcmp(str, "rmovChar") == 0);

    char str2[20] = "remove";
    removeChar(str2, 'e');
    assert(strcmp(str2, "rmov") == 0);

    char str3[20] = "";
    removeChar(str3, 'e');
    assert(strcmp(str3, "") == 0);
}

// Fonction permettant de tester la fonction getfield
void test_getfield() {
    char line[50] = "element1,element2,element3";
    char* result = getfield(line, 1);
    assert(strcmp(result, "element1") == 0);

    char line2[50] = "\"element1\",\"element2\",element3";
    result = getfield(line2, 2);
    assert(strcmp(result, "element2") == 0);

    char line3[50] = "element1";
    result = getfield(line3, 2);
    assert(result == NULL);
}

// Fonction permettant de tester la fonction get_elem
void test_get_elem() {
    char tmp[50] = "element1,element2,element3";
    char* result = get_elem(tmp, 1);
    assert(strcmp(result, "element1") == 0);

    char tmp2[50] = "\"element1\",\"element2\",element3";
    result = get_elem(tmp2, 2);
    assert(strcmp(result, "element2") == 0);

    char tmp3[50] = "element1";
    result = get_elem(tmp3, 2);
    assert(result == NULL);
}

// Fonction permettant de tester la fonction which_column
void test_which_column() {
    int* result = which_column("nahs");
    assert(result[0] == 1);
    assert(result[1] == 1);
    assert(result[2] == 1);
    assert(result[3] == 1);

    result = which_column("na");
    assert(result[0] == 1);
    assert(result[1] == 1);
    assert(result[2] == 0);
    assert(result[3] == 0);

    result = which_column("h");
    assert(result[0] == 0);
    assert(result[1] == 0);
    assert(result[2] == 1);
    assert(result[3] == 0);
}




// -------------- Test fichier json.c


void test_print_elem_json() {
    // Créer une chaîne de caractères qui représente une ligne de données
    char *line = "Zhongzhi Zhang,Fudan University,http://homepage.fudan.edu.cn/zhangzz,DrcEuSkAAAAJ'\n'";

    // Créer un tableau d'entiers pour spécifier les colonnes à imprimer
    int wanted_columns[] = {1, 0, 1, 1};

    // Imprimer les colonnes spécifiées en format JSON
    print_elem_json(wanted_columns, line, 0);

    // Vérifier que la sortie correspond à ce qui était attendu

    printf("\n\n");
}

void test_print_par_affiliation_json() {
    // Créer une chaîne de caractères qui représente une ligne de données
    char *line = "Zhongzhi Zhang,Fudan University,http://homepage.fudan.edu.cn/zhangzz,DrcEuSkAAAAJ'\n'";

    // Créer un tableau d'entiers pour spécifier les colonnes à imprimer
    int wanted_columns[] = {1, 0, 1, 1};

    // Imprimer les colonnes spécifiées en format JSON
    print_par_affiliation_json(wanted_columns, line, 0);

    // Vérifier que la sortie correspond à ce qui était attendu

    printf("\n\n");
}

void test_write_elem_json() {
    // Créer une chaîne de caractères qui représente une ligne de données
    char *line = "Zhongzhi Zhang,Fudan University,http://homepage.fudan.edu.cn/zhangzz,DrcEuSkAAAAJ'\n'";

    // Créer un tableau d'entiers pour spécifier les colonnes à écrire
    int wanted_columns[] = {1, 0, 1, 1};

    // Ouvrir un fichier pour écrire les données en format JSON
    FILE *f = fopen("test.json", "w");

    // Écrire les colonnes spécifiées en format JSON dans le fichier
    write_elem_json(wanted_columns, line, 0, f);

    // Fermer le fichier
    fclose(f);

    // Vérifier que le fichier a été créé et qu'il contient le contenu attendu

}

void test_write_par_affiliation_json() {
    // Créer une chaîne de caractères qui représente une ligne de données
    char *line = "Zhongzhi Zhang,Fudan University,http://homepage.fudan.edu.cn/zhangzz,DrcEuSkAAAAJ'\n'";

    // Créer un tableau d'entiers pour spécifier les colonnes à écrire
    int wanted_columns[] = {1, 0, 1, 1};

    // Ouvrir un fichier pour écrire les données en format JSON
    FILE *f = fopen("test.json", "w");

    // Écrire les colonnes spécifiées en format JSON dans le fichier
    write_par_affiliation_json(wanted_columns, line, 0, f);


}


void test_print_elem_yaml(){
  int wanted_columns[4] = {1, 1, 1, 1};
  char* line = "Zhongzhi Zhang,Fudan University,http://homepage.fudan.edu.cn/zhangzz,DrcEuSkAAAAJ";
  print_elem_yaml(wanted_columns, line);
  printf("\n\n");
}

void test_print_par_affiliation_yaml(){
  int wanted_columns[4] = {1, 0, 1, 1};
  char* line = "Zhongzhi Zhang,Fudan University,http://homepage.fudan.edu.cn/zhangzz,DrcEuSkAAAAJ";
  print_par_affiliation_yaml(wanted_columns, line);
  printf("\n\n");
}

void test_write_elem_yaml(){
  int wanted_columns[4] = {1, 1, 1, 1};
  char* line = "Zhongzhi Zhang,Fudan University,http://homepage.fudan.edu.cn/zhangzz,DrcEuSkAAAAJ";
  FILE* f = fopen("test.yaml", "w");
  write_elem_yaml(wanted_columns, line, f);
  fclose(f);
  f = fopen("test.yaml", "r");
  char buffer[1024];
  fgets(buffer, 1024, f);
  assert(strcmp(buffer, "  - name: \"Zhongzhi Zhang\"\n") == 0);
  fgets(buffer, 1024, f);
  assert(strcmp(buffer, "    affiliation: \"Fudan University\"\n") == 0);
  fgets(buffer, 1024, f);
  assert(strcmp(buffer, "    homepage: \"http://homepage.fudan.edu.cn/zhangzz\"\n") == 0);
  fgets(buffer, 1024, f);
  assert(strcmp(buffer, "    scholar: DrcEuSkAAAAJ\n") == 0);
  fclose(f);
  remove("test.yaml");
}

void test_write_par_affiliation_yaml(){
  int wanted_columns[4] = {1, 0, 1, 1};
  char* line = "Zhongzhi Zhang,Fudan University,http://homepage.fudan.edu.cn/zhangzz,DrcEuSkAAAAJ";
  FILE* f = fopen("test.yaml", "w");
  write_par_affiliation_yaml(wanted_columns, line, f);
  fclose(f);
  f = fopen("test.yaml", "r");
  char buffer[1024];
  fgets(buffer, 1024, f);
  assert(strcmp(buffer, "  - name : \"Zhongzhi Zhang\"\n") == 0);
  fgets(buffer, 1024, f);
  assert(strcmp(buffer, "    homepage : \"http://homepage.fudan.edu.cn/zhangzz\"\n") == 0);
  fgets(buffer, 1024, f);
  assert(strcmp(buffer, "    scholar: DrcEuSkAAAAJ\n") == 0);
  fclose(f);
  remove("test.yaml");
}




int main() {
    test_strip();
    test_removeChar();
    test_getfield();
    test_get_elem();
    test_which_column();
    //---------json
    test_print_elem_json();
    test_print_par_affiliation_json();
    test_write_elem_json();
    test_write_par_affiliation_json();
    //----------- yaml
    test_print_elem_yaml();
	test_print_par_affiliation_yaml();
	test_write_elem_yaml();
	test_write_par_affiliation_yaml();

    /*------------Test Integration----------------------------------*/
	printf("test integration\n");
	int par_aff = 0;
	//int first_row = 0;
	char *argument_t = "yaml";
  	char *argument_f= "ns";
  	char *argument_s= "University of Hildesheim";
  	char *argument_o = TEST_YAML;


	int* wanted_columns =  which_column(argument_f);


	FILE *fp;
	fp = fopen("csrankings.csv","r");

	if (fp == NULL){
		printf("Ouverture Impossible\n");
		exit(0);
	}
	FILE * outputFile;
	if (argument_o != NULL){// Si on écrit dans un fichier

		outputFile = fopen(argument_o,"w");// Ouverture du fichier
		if (outputFile == NULL){
			printf("Ouverture Impossible\n");
			exit(0);
		}
	}
	while( feof(fp) ==0 ){
		char line[1024];

		while (fgets(line, 1024, fp)){
			char* tmp = strdup(line);

			char* tmp2 = strdup(tmp);
			if (par_aff == 0){
				// Si affichage par affiliation
				fprintf(outputFile,"---\naffiliation :");
				fprintf(outputFile," \"%s\"",argument_s);
				fprintf(outputFile,"\n");
				fprintf(outputFile,"members :\n");
				par_aff = 1;
			}
			if (strcmp(getfield(tmp2, 2),argument_s)==0){
				write_par_affiliation_yaml(wanted_columns, tmp,outputFile);
			}
			free(tmp);
		}
	}

	if (strcmp(argument_t,"yaml") && argument_s != NULL && argument_o != NULL){
		fprintf(outputFile,"\n\t\t]\n\t}\n}\n}");
	}


	fclose(outputFile);

	fclose(fp);
	// Comparer le fichier YAML généré avec le fichier correct YAML
	FILE *fp1, *fp2;
	int c1, c2;

	fp1 = fopen("test.yaml", "r");
	fp2 = fopen("build/correct.yaml", "r");

	if (fp1 == NULL ) {
    		printf("Erreur lors de l'ouverture des fichiers de test 1\n");
    		exit(0);
	}
	if ( fp2 == NULL) {
    		printf("Erreur lors de l'ouverture des fichiers de test 2\n");
    		exit(0);
	}

	int files_are_equal = 1;
	//int pos = 0, line = 1;
	while (files_are_equal && ((c1 = getc(fp1)) != EOF) && ((c2 = getc(fp2)) != EOF)) {

   		if (c1 != c2) {
        		files_are_equal = 0;
        		printf("---%c-\n---%c-\n---", c1,c2);
        		printf("Les fichiers de test sont différents stop\n");
    		}
	}

	if (files_are_equal) {
	    printf("Les fichiers de test sont identiques\n");
	} else {
	    printf("Les fichiers de test sont différents\n");
	}

	fclose(fp1);
	fclose(fp2);



    printf("All tests pass\n");

    return 0;
}
