#include <json.h>
/*-------------------------------------------------------------------------

				   					JSON

-------------------------------------------------------------------------*/

//--------------- Print json -------------

void print_par_affiliation_json(int *wanted_columns, char*tmp, int fj){

    if (fj ==0){
        printf("\n\t\t{\n");
    }
    else {printf(",\n\t\t{\n");}
    if (wanted_columns[0] == 1){
      printf("\t\t\t\"name\" : \"");
      char * name = get_elem(tmp,1);
      printf("%s", name);
      if (wanted_columns[2] != 0 || wanted_columns[3] != 0){
      	printf("\",\n");
      }else {printf("\"\n");}
    }
    if (wanted_columns[2] == 1){
  		printf("\t\t\t\"homepage\" : \"");
      char *home = get_elem(tmp,3);
      printf("%s", home);
  		if ( wanted_columns[3] != 0){
          	printf("\",\n");
          }else {printf("\"\n");}
    }
    if (wanted_columns[3] == 1){
		char *tmp2 = strdup(tmp);
		char * id = (char * )getfield(tmp2,4);
		id[strlen(id) -1] = '\0';
		printf("\t\t\t\"scholar id\" : \"%s\"", id);
    }
    printf("\n\t\t}");

}
void print_elem_json(int *wanted_columns ,char* tmp, int fj){

    // ------ Si c'est le premier on met pas la virgule si non on la met
    if (fj ==0){
        printf("[\n{\n");
    }
    else {printf(",\n{\n");}

    if (wanted_columns[0] == 1){
      //printf("n tmp %s\n", tmp);
      printf("\t\"name\": \"");
      char * name = get_elem(tmp,1);
      printf("%s", name);
      if (wanted_columns[1] != 0 || wanted_columns[2] != 0 || wanted_columns[3] != 0){
      	printf("\",\n");
      }else {printf("\"\n");}
    }
    if (wanted_columns[1] == 1){
  		printf("\t\"affiliation \": \"");
  		char * affil = get_elem(tmp,2);
      printf("%s",affil);
  		if (wanted_columns[2] != 0 || wanted_columns[3] != 0){
          	printf("\",\n");
          }else {printf("\"\n");}
    }
    if (wanted_columns[2] == 1){
  		printf("\t\"homepage\": \"");
      char *home = get_elem(tmp,3);
      printf("%s", home);

  		if ( wanted_columns[3] != 0){
          	printf("\",\n");
          }else {printf("\"\n");}
    }
    if (wanted_columns[3] == 1){


		char *tmp2 = strdup(tmp);
		char * id = (char * )getfield(tmp2,4);
		id[strlen(id) -1] = '\0';
		printf("\t\"scholar id\" : \"%s\"", id);
    }
    printf("\n}");
    //printf("\n");


}

//--------------- Write json -------------


void write_elem_json(int *wanted_columns ,char* tmp, int fj, FILE *f){
    // ------ Si c'est le premier on met pas la virgule si non on la met
    if (fj ==0){
        fprintf(f,"[\n{\n");
    }
    else {fprintf(f,",\n{\n");}

    if (wanted_columns[0] == 1){
        //printf("n tmp %s\n", tmp);
        fprintf(f,"\t\"name\": \"");
        char * name = get_elem(tmp,1);
        fprintf(f,"%s", name);
        if (wanted_columns[1] != 0 || wanted_columns[2] != 0 || wanted_columns[3] != 0){
        	fprintf(f,"\",\n");
        }else {fprintf(f,"\"\n");}
    }
    if (wanted_columns[1] == 1){
  		fprintf(f,"\t\"affiliation\": \"");
      char * affil = get_elem(tmp,2);
      fprintf(f,"%s", affil);

  		if (wanted_columns[2] != 0 || wanted_columns[3] != 0){
        fprintf(f,"\",\n");
      }else {fprintf(f,"\"\n");}
    }
    if (wanted_columns[2] == 1){
		fprintf(f,"\t\"homepage\": \"");
    char * home = get_elem(tmp,3);
    fprintf(f,"%s", home);
  		if ( wanted_columns[3] != 0){
          	fprintf(f,"\",\n");
        }else {fprintf(f,"\"\n");}
    }
    if (wanted_columns[3] == 1){


		char *tmp2 = strdup(tmp);
		char * id = (char * )getfield(tmp2,4);
		id[strlen(id) -1] = '\0';
		fprintf(f,"\t\"scholar id\" : \"%s\"", id);
    }
    fprintf(f,"\n}");

}

void write_par_affiliation_json(int *wanted_columns, char*tmp, int fj, FILE * f){
    if (fj ==0){
        fprintf(f,"\n\t\t{\n");
    }
    else {fprintf(f,",\n\t\t{\n");}
    if (wanted_columns[0] == 1){
      fprintf(f,"\t\t\t\"name\" : \"");
      char * name = get_elem(tmp,1);
      fprintf(f,"%s", name);
      if (wanted_columns[2] != 0 || wanted_columns[3] != 0){
      	fprintf(f,"\",\n");
      }else {fprintf(f,"\"\n");}
    }if (wanted_columns[2] == 1){
  		fprintf(f,"\t\t\t\"homepage\" : \"");
      char * home = get_elem(tmp,3);
      fprintf(f,"%s", home);
  		if ( wanted_columns[3] != 0){
          	fprintf(f,"\",\n");
          }else {fprintf(f,"\"\n");}
    }
    if (wanted_columns[3] == 1){
  		char *tmp2 = strdup(tmp);
  		char * id = (char * )getfield(tmp2,4);
  		id[strlen(id) -1] = '\0';
  		fprintf(f,"\t\t\t\"scholar id\" : \"%s\"", id);
    }
    fprintf(f,"\n\t\t}");
}
