#include <fonctionsBases.h>


//----------- Fonctions pour nettoyer le texte récupéré dans le csrankings------------

// Enlever les tabulations d'une chaine de caractère
void strip(char *s) {
    char *p2 = s;
    while(*s != '\0') {
        if(*s != '\t' ) {
            *p2++ = *s++;
        } else {
            ++s;
        }
    }
    *p2 = '\0';
}

// Enlever caractère d'un string
void removeChar(char *str, char garbage) {

    char *src, *dst;
    for (src = dst = str; *src != '\0'; src++) {
        *dst = *src;
        if (*dst != garbage) dst++;
    }
    *dst = '\0';
}

// Découper la ligne
char* getfield(char* line, int num)
{
    char* tok;
    for (tok = strtok(line, ",\n"); tok && *tok; tok = strtok(NULL, ",\n")){
        if (!--num){
            while (strchr(tok,'\"') != NULL){
              removeChar(tok,'"');
            }
            while (strchr(tok,'\t') != NULL){
              strip(tok);
            }
            return tok;
        }
    }
    return NULL;
}


// Recuperer element dans la ligne
char * get_elem(char *tmp, int num ){
  char *tmp2 = strdup(tmp);
  return getfield(tmp2, num);
}



// Fonction permettant de savoir les éléments a recuperer dans l'affichage, soit la commande "-f"

int* which_column(char* arg_f){
	int *res = malloc(sizeof(int)*4); // Vecteur de taille 4 pour chaque lettre n - a - h -s
	if (strpbrk(arg_f,"n")!= NULL){ // Si la lettre n est ecrite
		res[0]= 1;
	}
	else {res[0] = 0;}
	if (strpbrk(arg_f,"a")!= NULL){// Si la lettre a est ecrite
		res[1]= 1;
	}
	else {res[1] = 0;}
	if (strpbrk(arg_f,"h")!= NULL){// Si la lettre h est ecrite
		res[2]= 1;
	}
	else {res[2] = 0;}
	if (strpbrk(arg_f,"s")!= NULL){// Si la lettre s est ecrite
		res[3]= 1;
	}
	else {res[3] = 0;}


	return res;

}
