#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <yaml.h>
#include <json.h>
#include <fonctionsBases.h>
#define MAXCHAR 1000




/*-------------------------------------------------------------------------
-------------------------------------------------------------------------
-------------------------------------------------------------------------*/



int main(int argc,char *argv[]) {

	int first_json = 0;
	int par_aff = 0;
	int first_row = 0;
	char *argument_t = "yaml";
	char *argument_f;
	char *argument_s= NULL;
	char *argument_o = NULL;
	//récupérer les entrées
	printf("%d\n", argc);
	for (int i = 0; i < (argc - 1); i++){
		if (strcmp(argv[i],"-t")==0){
			argument_t = argv[i+1];
		}
		if (strcmp(argv[i],"-f")==0){
			argument_f = argv[i+1];
		}
		if (strcmp(argv[i],"-s")==0){
			argument_s = argv[i+1];
		}
		if (strcmp(argv[i], "-o")==0){
			argument_o = argv[i+1];
		}
	}

	printf("t =  %s,f =  %s ,s = %s, o = %s \n", argument_t, argument_f,argument_s, argument_o);
	int* wanted_columns =  which_column(argument_f);


	FILE *fp;
	fp = fopen("csrankings.csv","r");

	if (fp == NULL){
		printf("Ouverture Impossible\n");
		exit(0);
	}
	FILE * outputFile;
	if (argument_o != NULL){// Si on écrit dans un fichier

		outputFile = fopen(argument_o,"w");// Ouverture du fichier
		if (outputFile == NULL){
			printf("Ouverture Impossible\n");
			exit(0);
		}
	}
	while( feof(fp) ==0 ){
		char line[1024];

		while (fgets(line, 1024, fp)){
			char* tmp = strdup(line);
			if (argument_o != NULL){
			// Si on écrit dans un fichier


				if (strcmp(argument_t, "json")==0){
				// Si json
					if (argument_s != NULL){ // Si par affiliation
						char* tmp2 = strdup(tmp);
						if (par_aff == 0){
							fprintf(outputFile,"{\n\t\"affiliation\" : {\"");
							fprintf(outputFile," %s\"",argument_s);
							fprintf(outputFile,": { \n");
							fprintf(outputFile,"\t\t\"members\" : [ \n");
							par_aff = 1;
						}
						if (strcmp(getfield(tmp2, 2),argument_s)==0){
							write_par_affiliation_json(wanted_columns, tmp,first_json,outputFile);
							first_json = first_json + 1;
						}
					}
					else {
						write_elem_json(wanted_columns,tmp, first_json,outputFile);
						first_json = first_json + 1;
					}
		    	}
		    	else{
		    	// Si pas json ie yaml

					if (argument_s != NULL){
						char* tmp2 = strdup(tmp);
						if (par_aff == 0){
						// Si affichage par affiliation
							fprintf(outputFile,"---\naffiliation :");
							fprintf(outputFile," \"%s\"",argument_s);
							fprintf(outputFile,"\n");
							fprintf(outputFile,"members : \n");
							par_aff = 1;
						}
						if (strcmp(getfield(tmp2, 2),argument_s)==0){
							write_par_affiliation_yaml(wanted_columns, tmp,outputFile);
						}
					}
					else {
						if (first_row ==0){
							fprintf(outputFile,"---\n");
							first_row = 1;}

							write_elem_yaml(wanted_columns,tmp,outputFile);
						//printf("yaml\n");
					}
				}
				// NOTE strtok clobbers tmp
				free(tmp);

			}
			else {

				if (strcmp(argument_t, "json")==0){
					if (argument_s != NULL){
					// Par affiliation json
						char* tmp2 = strdup(tmp);
						if (par_aff == 0){
							printf("{\n\t\"affiliation\" : {\"");
							printf(" %s\"",argument_s);
							printf(": { \n");
							printf("\t\t\"members\" : [ \n");
							par_aff = 1;
						}
						if (strcmp(getfield(tmp2, 2),argument_s)==0){

							print_par_affiliation_json(wanted_columns, tmp,first_json);
							first_json = first_json + 1;
						}

					}
					else {
						print_elem_json(wanted_columns,tmp, first_json);
						first_json = first_json + 1;
					}
		    	}
		    	else{
					if (argument_s != NULL){
						char* tmp2 = strdup(tmp);
						if (par_aff == 0){
							printf("---\naffiliation :");
							printf(" \"%s\"",argument_s);
							printf("\n");
							printf("members : \n");
							par_aff = 1;
						}
						if (strcmp(getfield(tmp2, 2),argument_s)==0){
						print_par_affiliation_yaml(wanted_columns, tmp);
						}
					}
					else {
						if (first_row ==0){
							printf("---\n");
							first_row = 1;}
							print_elem_yaml(wanted_columns,tmp);
						}
				}
				// NOTE strtok clobbers tmp
				free(tmp);
			}
			//printf("end\n");
		}
		//printf("outof while\n");

	}
	if (strcmp(argument_t,"yaml") && argument_s != NULL && argument_o == NULL){
		printf("\n\t\t]\n\t}\n}\n}");
	}
	if (strcmp(argument_t,"yaml") && argument_o == NULL && argument_s == NULL){
		printf("\n]\n");
	}
	if (strcmp(argument_t,"yaml") && argument_s != NULL && argument_o != NULL){
		fprintf(outputFile,"\n\t\t]\n\t}\n}\n}");
	}
	if (strcmp(argument_t,"yaml") && argument_o != NULL && argument_s == NULL){
		fprintf(outputFile,"\n]\n");
	}
	if (argument_o != NULL){
		fclose(outputFile);
	}
	fclose(fp);


	return 0;
}
