#include <yaml.h>

/*-------------------------------------------------------------------------

				   					YAML

-------------------------------------------------------------------------*/

//------------ Print yaml --------------------
void print_elem_yaml(int *wanted_columns ,char* tmp){
    if (wanted_columns[0] == 1){
        //printf("n tmp %s\n", tmp);
        printf("  - name: \"");
        char * name = get_elem(tmp,1);
        printf("%s", name);
        printf("\"\n");
    }
    if (wanted_columns[1] == 1){
    	printf("    affiliation: \"");
      char *affil = get_elem(tmp,2);
      printf("%s", affil);
    	printf("\"\n");
    }
    if (wanted_columns[2] == 1){
    	printf("    homepage: \"");
      char *home = get_elem(tmp,3);
      printf("%s", home);
    	printf("\"\n");
    }
    if (wanted_columns[3] == 1){
    	printf("    scholar: ");
      char * schol = get_elem(tmp,4);
      printf("%s", schol);
    	printf("\n");
    }
}


void print_par_affiliation_yaml(int *wanted_columns, char*tmp){
    if (wanted_columns[0] == 1){

      printf("  - name : \"");
      char * name = get_elem(tmp,1);
      printf("%s", name);
      printf("\"\n");
    }if (wanted_columns[2] == 1){
      printf("    homepage : \"");
      char *home = get_elem(tmp,3);
      printf("%s", home);
    	printf("\"\n");
    }
    if (wanted_columns[3] == 1){
    	printf("    scholar: ");
      char * schol = get_elem(tmp,4);
      printf("%s", schol);
    	printf("\n");

    }

}

//------------ Write in yaml --------------------

void write_elem_yaml(int *wanted_columns, char* tmp, FILE* f){
	if (wanted_columns[0] == 1){
      //printf("n tmp %s\n", tmp);
      fprintf(f,"  - name: \"");
      char * name = get_elem(tmp,1);
      fprintf(f,"%s", name);
      fprintf(f,"\"\n");
    }
    if (wanted_columns[1] == 1){
    	fprintf(f,"    affiliation: \"");
      char * affil = get_elem(tmp,2);
      fprintf(f,"%s", affil);
    	fprintf(f,"\"\n");
    }
    if (wanted_columns[2] == 1){
    	fprintf(f,"    homepage: \"");
      char * home = get_elem(tmp,3);
      fprintf(f,"%s", home);
    	fprintf(f,"\"\n");
    }
    if (wanted_columns[3] == 1){
    	fprintf(f,"    scholar: ");
      char * schol = get_elem(tmp,4);
      fprintf(f,"%s", schol);
    	fprintf(f,"\n");
    }
}



void write_par_affiliation_yaml(int *wanted_columns, char*tmp, FILE * f){

  if (wanted_columns[0] == 1){

    fprintf(f,"  - name : \"");
    char * name = get_elem(tmp,1);
    fprintf(f,"%s", name);
    fprintf(f,"\"\n");
  }if (wanted_columns[2] == 1){
		fprintf(f,"    homepage : \"");
	  char * home = get_elem(tmp,3);
	  fprintf(f,"%s", home);
		fprintf(f,"\"\n");
  }
  if (wanted_columns[3] == 1){
		fprintf(f,"    scholar: ");
	  char * schol = get_elem(tmp,4);
	  fprintf(f,"%s", schol);
		fprintf(f,"\n");
  }
}
